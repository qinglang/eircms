<?php 

class Controller_post extends CI_Controller
{
	// Show main page.
	public function action_index()
	{
		$this->myview->setTheme('default');
		// $posts = $this->model_post->get(10);
		$this->myview->show("");
	}

	// Show all posts
	public function action_posts()
	{
		$number = $this->mydbconfig->get("default_result");
		$posts = $this->db->get('posts',$number);
		$data['posts'] = $posts;
		$this->myview->show('posts',$data);
	}
}