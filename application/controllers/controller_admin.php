<?php 

class Controller_admin extends CI_Controller
{
	public $current_user;
	public function __construct()
	{
		parent::__construct();
		$this->myview->setTheme("admin");
		$restricted_methods = array('action_index','action_post_new',"action_logout",
			"action_post_create", "action_post_edit", "action_post_update", "action_post_delete"
			);
		$necessary_methods = array('action_login','action_register');
		$this->myauth->auth($restricted_methods,'login');
		$this->myauth->noauth($necessary_methods,'admin');

		// libraries
		$this->load->library("form_validation");
	}

	function action_index()
	{
		// select all except content
		$this->db->select("id,title,category_id,user_id,created,updated,is_published,slug");
		$this->db->where("is_deleted",0);
		// order by created
		$this->db->order_by("created","desc");
		// get only 5 last post
		$data["last_posts"] = $this->db->get("posts",5);
		$this->myview->show("",$data);

	}

	public function action_post_new()
	{
		$data["categories"] = $this->db->get("categories");
		$this->myview->show('form_post',$data);
	}

	public function action_post_create()
	{
		if ($this->input->post())
		{
			$this->form_validation->set_rules("title","Title","required");
			$this->form_validation->set_rules("content","Content","required");

			if ($this->form_validation->run() == FALSE)
			{
				// something wrong
				$data["categories"] = $this->db->get("categories");
				$this->myview->show("form_post",$data);
			}
			else
			{
				// set the slug
				if ($this->input->post('slug'))
				{
					$slug = $this->input->post("slug");
				}
				else {
					$slug = url_title($this->input->post('title'),'-',TRUE);
				}

				$data = array(
					"title" => $this->input->post("title"),
					"content" => $this->input->post("content"),
					"slug" => $slug,
					"user_id" => current_user("id"),
					"created" => date("Y-m-d H:i:s"),
					"updated" => date("Y-m-d H:i:s"),
					"category_id" => $this->input->post("category_id"),
					"is_page" => 0,
					"is_published" => ($this->input->post("submit")=="publish" ? 1 : 0)
				);
				$this->model_post->post_new($data);

				$this->session->set_flashdata("success","Post successfully ".($this->input->post("submit")=="publish" ? "published" : "saved"));
				redirect(urlto('admin/post/all'));
			}
		}
		else {
			redirect("admin");
		}
	}

	public function action_post_all()
	{
		$this->db->where("is_deleted",0);
		$this->db->order_by("created","desc");
		$data["posts"] = $this->db->get("posts");
		$data["posts_count"] = $this->db->count_all("posts");
		$this->myview->show("all_post",$data);
	}

	public function action_post_edit()
	{
		$id = $this->uri->segment(4);
		$data["post"] = $this->db->get_where("posts",array("id"=>$id),1)->row();
		$data["categories"] = $this->db->get("categories");
		$this->myview->show("form_post",$data);
	}

	public function action_post_update()
	{
		if ($this->input->post())
		{
			$id = $this->uri->segment(4);
			$this->form_validation->set_rules("title","Title","required");
			$this->form_validation->set_rules("content","Content","required");
			if ($this->form_validation->run() == TRUE)
			{
				// 
				if ($this->input->post('slug'))
				{
					$slug = $this->input->post("slug");
				}
				else {
					$slug = url_title($this->input->post('title'),'-',TRUE);
				}

				$data = array(
					"id" => $id,
					"title" => $this->input->post("title"),
					"content" => $this->input->post("content"),
					"slug" => $slug,
					"user_id" => current_user("id"),
					"updated" => date("Y-m-d H:i:s"),
					"is_published" => ($this->input->post("submit")=="publish" ? 1 : 0),
					"category_id" => $this->input->post("category_id")
				);
				$this->model_post->update($id,$data);

				$this->session->set_flashdata("success","Your post successfully updated");
				redirect(urlto("admin/post/all"));
			}
			else
			{
				$data["categories"] = $this->db->get("categories");
				$data["post"] = $this->db->get_where("posts",array("id"=>$id),1)->row();
				$this->myview->show("form_post",$data);
			}
		}
		else
		{
			if (current_user()){
				redirect(urlto("admin"));
			}
			else {
				redirect(urlto());
			}
		}
	}

	// not really delete the post, just move it into the trash
	public function action_post_delete()
	{
		$id = $this->uri->segment(4);
		$post = $this->db->get_where("posts",array("id"=>$id),1);
		if ($post->num_rows()>0)
		{
			// move it into trash
			$this->db->where("id",$id);
			$this->db->update("posts",array("is_deleted"=>1));

			$this->session->set_flashdata("info","Post ".$post->row()->title." deleted!");
			redirect(urlto("admin/post/all"));
		}
		else {
			// error post not exists!
			$this->session->set_flashdata("error","Post not exists!");
			redirect(urlto("admin/post/all"));
		}
	}

	public function action_login()
	{
		$this->myview->setTheme('admin/auth');
		$data = array();
		$custom = array(
				"header" => NULL,
				"footer" => NULL
			);
		$this->myview->show('login',$data, $custom);
	}
	public function action_register()
	{
		$this->myview->setTheme('admin/auth');
		$data = array();
		$custom = array(
				"header" => NULL,
				"footer" => NULL
			);
		$this->myview->show('register',$data, $custom);
	}

	public function action_logout()
	{
		$this->session->unset_userdata('current_user');
		$this->session->set_flashdata('success',"Logged out successfully");
		redirect(urlto(''));
	}

	public function action_categories()
	{
		$this->db->order_by("id","desc");
		$data["categories"] = $this->db->get("categories");
		$this->myview->show("categories",$data);
	}

	public function action_category_new()
	{
		if ($this->input->post())
		{
			if ($this->input->post("name")){
				// slug
				$slug = ($this->input->post("slug") ? $this->input->post("slug") : url_title($this->input->post("name"),"_",TRUE));

				$data = array(
					"name" => $this->input->post("name"),
					"slug" => $slug,
					"description" => $this->input->post("description"),
					"user_id" => current_user("id")
					);
				$this->db->insert("categories",$data);
				$this->session->set_flashdata("success","Category '".$data["name"]."' created!");
				redirect(urlto("admin/categories"));
			}
			else {
				$this->session->set_flashdata("error","You must put name for category!");
				redirect(urlto("admin/categories"));
			}
		}
		else
		{
			redirect(urlto("admin/categories"));
		}
	}
}