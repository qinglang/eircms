<?php 

class Controller_auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->myauth->noauth(array("action_login","action_register"),"admin");
		$this->load->library("form_validation");
	}
	public function action_login()
	{
		$this->form_validation->set_rules("username","Username",'required');
		$this->form_validation->set_rules("password","Password",'required');
		if ($this->form_validation->run() == FALSE)
		{
			$this->myview->setTheme('admin/auth');
			$this->myview->show('login',array(),array("header"=>NULL,"footer"=>NULL));
		}
		else
		{
			$this->load->library('encrypt');
			$userdata = array(
					"username" => $this->input->post("username"),
					"password" => $this->encrypt->sha1($this->input->post("password"))
				);
			if ($this->db->get_where('users',$userdata)->num_rows() > 0)
			{
				// Correct
				$this->session->set_flashdata("success","Welcome, ".$userdata["username"]."!");
				$this->session->set_userdata('current_user',$userdata['username']);
				redirect(urlto('admin'));
			}
			else
			{
				// False
				$this->session->set_flashdata("error","User is not exist!");
				redirect(urlto('login'));
			}
		}
	}

	public function action_register()
	{
		// using library encrypt for encrypting password
		$this->load->library('encrypt');

		// validation rule
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password_check', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');

		if ($this->form_validation->run() == FALSE){
			$this->myview->setTheme('admin/auth');
			$this->myview->show('register',array(),array("header"=>NULL,"footer"=>NULL));
		}else {
			// check user exist?
			$post = array("username"=>$this->input->post('username'),"email"=>$this->input->post('email'));
			$result = $this->db->get_where('users',$post);
			if ($result->num_rows()> 0)
			{
				$this->session->set_flashdata('error',"Username or email already exist!");
				redirect(urlto('register'));
			}
			else
			{
				// saving data
				if (!$this->db->get_where('users',array("role"=>"admin"))->num_rows() > 0)
				{
					$role = "admin";
				}
				else {
					$role = "user";
				}
				$userdata = array(
						"username" => $this->input->post('username'),
						"email" => $this->input->post('email'),
						"password" => $this->encrypt->sha1($this->input->post('password')),
						"role" => $role,
						"created" => date("Y-m-d H:i:s")
					);
				$this->db->insert("users",$userdata);
				$this->session->set_userdata("current_user",$userdata['username']);

				// redirect
				$this->session->set_flashdata('success',"User ".$userdata['username']." created!");
				redirect(urlto());

			}
		}
	}
}