<div class="content">
	<div class="row-fluid">
		<div class="span12">
			<h3>All Posts</h3>
			<nav class="admin-nav">
				<a href="<?php echo urlto('admin/post/new') ?>">new post</a>
				<a href="<?php echo urlto('admin/post/trash') ?>">trash</a>
				There are <?php echo $posts_count; ?> post(s).
			</nav>
			<?php if ($posts->num_rows()>0):  ?>
				<ul class="posts post-list list">
					<?php foreach($posts->result() as $post): ?>
						<li class="post">
							<div class="row-fluid">
								<div class="span4"><span class="post-title"><a href="<?php echo urlto('admin/post/edit/'.$post->id); ?>"><?php echo $post->title; ?></a></span></div>
								<div class="span4">
									<span class="post-meta">
										by <a href="<?php echo urlto("admin/post/all/user/".get_username_by_id($post->user_id)) ?>"><?php echo get_username_by_id($post->user_id); ?></a> 
										at 2013/02/28 on <a href="#"><?php echo get_category_by_id($post->category_id); ?></a>
										<?php if ($post->is_published == 0): ?>[Unpublished] <?php endif; ?>
									</span>
								</div>
								<div class="span4">
									<span class="post-meta-action">
										<a href="<?php echo urlto("admin/post/edit/".$post->id) ?>" class="btn btn-small">edit</a>
										<a href="<?php echo urlto("admin/post/delete/".$post->id); ?>" class="btn btn-small">trash</a>
										<a href="<?php echo urlto("post/".$post->id."/".$post->slug) ?>" class="btn btn-small">view</a>
									</span>
								</div>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php else: ?>
			<p>There is no posts available</p>
			<?php endif; ?>
		</div>
	</div>
</div>