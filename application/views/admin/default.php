<div class="content">
	<div class="row-fluid">
		<div class="span6">
			<h3>Latest Posts</h3>
			<nav class="admin-nav">
				<a href="<?php echo urlto('admin/post/new') ?>">new post</a>
				<?php if ($last_posts->num_rows()>0):  ?>
					<a href="<?php echo urlto('admin/post/all') ?>">all post</a>
				<?php endif; ?>
			</nav>
			<?php if ($last_posts->num_rows()>0):  ?>
				<ul class="posts post-list list">
					<?php foreach($last_posts->result() as $post): ?>
						<li class="post">
							<span class="post-title"><a href="<?php echo urlto('admin/post/edit/'.$post->id); ?>"><?php echo $post->title; ?></a></span><br>
							<span class="post-meta">by <a href="#"><?php echo get_username_by_id($post->user_id); ?></a> at 2013/02/28 on <a href="#"><?php echo get_category_by_id($post->category_id); ?></a></span>
							<span class="post-meta-action"><a href="<?php echo urlto("admin/post/edit/".$post->id) ?>" class="btn btn-small">edit</a> <a href="<?php echo urlto("admin/post/delete/".$post->id) ?>" class="btn btn-small">trash</a> <a href="<?php echo urlto("post/".$post->id."/".$post->slug) ?>" class="btn btn-small">view</a></span>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php else: ?>
			<p>There is no posts available</p>
			<?php endif; ?>
		</div>
		<div class="span6">
			<h3>Latest Comments</h3>
			<p>This is latest unapproved comments</p>
			<nav class="admin-nav"><a href="<?php echo urlto('admin/comments') ?>">all comments</a></nav>
			<ul class="list comments">
				<li>
					<span class="comment-sub"><a href="">comment preview on here with [...]</a></span>
					<span class="comment-meta">in post <a href="">Hello World</a></span>
				</li>
			</ul>
		</div>
	</div>
</div>