<!doctype html>

<html>
<head>
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php echo getCSS('styles.css') ?>
</head>
<body>
	<div class="container">
		<div class="row-fluid">
			<div class="span4 offset4">
				<h3>Login</h3>
				<?php if ($this->session->flashdata('error')): ?>
				<div class="alert alert-error"><?php echo $this->session->flashdata('error')  ?></div>
				<?php elseif ($this->session->flashdata('success')): ?>
				<div class="alert alert-success"><?php echo $this->session->flashdata('success') ?></div>
				<?php elseif ($this->session->flashdata('info')): ?>
				<div class="alert alert-info"><?php echo $this->session->flashdata('info'); ?></div>
				<?php endif; ?>
				<!-- validation errors -->
				<?php if (validation_errors()): ?>
				<div class="alert alert-error">
					<?php echo validation_errors(); ?>
				</div>
				<?php endif; ?>
				<!-- end validation errors -->
				<?php echo form_open('auth/login') ?>
					<p><input type="text" name="username" placeholder="username" class="span12"></p>
					<p><input type="password" name="password" placeholder="password" class="span12"></p>
					<p>
						<button class="btn btn-primary" type="submit">Login</button>
						<a href="<?php echo urlto() ?>" class="btn">cancel</a>
					</p>
					<p><a href="<?php echo urlto('register') ?>">Don't have an account yet?</a></p>
				</form>
			</div>
		</div>
	</div>
</body>
</html>