<!doctype html>

<html>
<head>
	<title><?php echo $site_name; ?> - Admin</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="<?php echo getResource('deva','css'); ?>">
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
</head>
<body>
	<div class="container">
		<header>
			<div class="row-fluid main-nav">
				<div class="span8">
					<nav>
						<a class="site-title" href="<?php echo urlto('admin') ?>"><?php echo $site_name; ?></a>
						<a href="javascript:;" class="open-menu" data-nav="content">content</a>
						<a href="javascript:;" class="open-menu" data-nav="media">media</a>
						<a href="javascript:;" class="open-menu" data-nav="other">other</a>
					</nav>
				</div>
				<div class="span4">
					<nav class="text-right">
						<a href="<?php echo urlto() ?>">view</a>
						<a href="javascript:;" class="open-menu" data-nav="user">hi, <?php echo current_user("username"); ?></a>
						<a href="<?php echo urlto('admin/logout'); ?>">logout</a>
					</nav>
				</div>
			</div>
			<div class="sub-main-nav">
				<div class="main-nav-cat" id="mainnav-site">
					<nav>
						<a href="<?php echo urlto(); ?>">visit site</a>
					</nav>
				</div>
				<div class="main-nav-cat" id="mainnav-content">
					<nav>
						<a href="<?php echo urlto('admin/post/new') ?>">new post</a>
						<a href="<?php echo urlto('admin/post/all') ?>">all posts</a>
						<a href="<?php echo urlto('admin/page/new') ?>">new pages</a>
						<a href="<?php echo urlto('admin/page/all') ?>">all pages</a>
						<a href="<?php echo urlto('admin/comments') ?>">comments</a>
						<a href="<?php echo urlto('admin/categories') ?>">categories</a>
					</nav>
				</div>
				<div class="main-nav-cat" id="mainnav-media">
					<nav>
						<a href="<?php echo urlto('admin/media/upload') ?>">upload</a>
						<a href="<?php echo urlto('admin/media/all') ?>">media</a>
					</nav>
				</div>
				<div class="main-nav-cat" id="mainnav-other">
					<nav>
						<a href="<?php echo urlto('admin/settings') ?>">settings</a>
						<a href="<?php echo urlto('admin/forum') ?>">forum</a>
						<a href="<?php echo urlto('admin/gallery') ?>">galleries</a>
						<a href="<?php echo urlto('admin/page/home') ?>">homepage</a>
					</nav>
				</div>
				<div class="main-nav-cat" id="mainnav-user">
					<nav class="text-right">
						<a href="<?php echo urlto('users/'.current_user('username')) ?>"><?php echo current_user() ?></a>
						<a href="<?php echo urlto('admin/user/edit/'.current_user('username')); ?>">edit profile</a>
						<a href="<?php echo urlto('admin/user/all') ?>">all users</a>
					</nav>
				</div>
			</div>
		</header>
		<?php if ($this->session->flashdata('error')): ?>
		<div class="alert alert-flash alert-error"><?php echo $this->session->flashdata('error')  ?></div>
		<?php elseif ($this->session->flashdata('success')): ?>
		<div class="alert alert-flash alert-success"><?php echo $this->session->flashdata('success') ?></div>
		<?php elseif ($this->session->flashdata('info')): ?>
		<div class="alert alert-flash alert-info"><?php echo $this->session->flashdata('info'); ?></div>
		<?php endif; ?>