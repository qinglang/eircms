		<footer>
			<div class="row-fluid">
				<div class="span12">
					<p>
						&copy; <?php echo date("Y"); ?> <?php echo $company_name; ?>
						|
						<a href="<?php echo urlto('login') ?>">Login</a> | <a href="<?php echo urlto('register') ?>">Register</a>
					</p>
					<?php if (ENVIRONMENT == "development"): ?>
					<h4>Debugging</h4>
					<p>
						<strong>Controller</strong>: <?php echo $this->uri->rsegment(1); ?>
						<strong>Method</strong>: <?php echo $this->uri->rsegment(2); ?>
					</p>
					<p><strong>Sessions</strong>:</p>
					<ul>
						<?php foreach ($this->session->all_userdata() as $name => $content): ?>
						<li><?php echo $name ?>:<?php echo $content ?></li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
				</div>
			</div>
		</footer>
	</div>
</body>
</html>