<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "controller_post/action_index";
$route['404_override'] = '';

// auth
$route['login'] = "controller_admin/action_login";
$route['register'] = "controller_admin/action_register";
$route['auth/register'] = "controller_auth/action_register";
$route['auth/login'] = "controller_auth/action_login";

// admin related
$route["admin/post/([a-z0-9\_]+)(/(:num))?"] = "controller_admin/action_post_$1";
$route['admin'] = "controller_admin/action_index";
$route['admin/post/([a-z0-9\_]+)'] = "controller_admin/action_post_$1";
$route['admin/post/edit/(:num)'] = "controller_admin/action_post_edit";
$route['admin/post/update/(:num)'] = "controller_admin/action_post_update";
$route['admin/([a-z0-9\_]+)'] = "controller_admin/action_$1";


/* End of file routes.php */
/* Location: ./application/config/routes.php */