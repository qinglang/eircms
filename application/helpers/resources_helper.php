<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// getCSS
// print link tag to a css
if ( !function_exists('getCSS')) {

	function getCSS($filename)
	{
		return '<link rel="stylesheet" type="text/css" href="'.base_url().'resources/stylesheets/'.$filename.'">';
	}
}

if ( !function_exists("getJS")) {

	function getJS($filename)
	{
		return '<script type="text/javascript" src="'.base_url().'resources/javascripts/'.$filename.'"></script>';
	}
}

if ( !function_exists('getResource')) {
	function getResource($filename,$filetype='')
	{
		// if filetype empty, then look in file
		switch ($filetype) {
			case 'js':
				$dir = "javascripts";
				break;
			case 'css':
				$dir = "stylesheets";
				break;
			case 'img':
				$dir = "images";
				break;
			default:
				$dir = "files";
				break;
		}

		if ($filetype == 'js'||$filetype=='css'||$filetype=='img') {
			$ext = ".".$filetype;
		}

		return base_url().'resources/'.$dir.'/'.$filename.$ext;
	}
}

if (!function_exists('urlto')){
	function urlto($url='',$toOut=FALSE)
	{
		if ($toOut){
			return $url;
		}
		else {
			$ip = index_page();
			if (!empty($ip)) {
				return base_url().index_page().'/'.$url;
			}
			else {
				return base_url().$url;
			}
		}
	}
}

if (!function_exists("get_username_by_id"))
{
	function get_username_by_id($id)
	{
		$CI =& get_instance();
		return $CI->model_user->get_username_by_id($id);
	}
}

if (!function_exists("get_category_by_id"))
{
	function get_category_by_id($id)
	{
		$CI =& get_instance();
		return $CI->model_user->get_category_by_id($id);
	}
}