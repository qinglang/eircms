<?php 


class Model_user extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function user_new()
	{

	}

	public function get_username_by_id($id)
	{
		$this->db->select("username");
		return $this->db->get_where("users",array("id"=>$id),1)->row()->username;
	}

	public function get_category_by_id($id)
	{
		$this->db->select("name");
		return $this->db->get_where("categories",array("id"=>$id),1)->row()->name;
	}
}