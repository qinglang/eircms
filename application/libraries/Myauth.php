<?php 

class Myauth
{
	public $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library(array('session'));
	}

	// only registered can access
	public function auth($methods=array(),$target="")
	{
		$current_method = $this->CI->uri->rsegment(2);

		if (in_array($current_method, $methods)) {
			if (!$this->CI->session->userdata('current_user')) {
				$this->CI->session->set_flashdata('error',"You must be logged in!");
				redirect(urlto($target));
			}
		}
	}

	// logged in user are necessary
	public function noauth($methods=array(),$target="")
	{
		$current_method = $this->CI->uri->rsegment(2);
		if (in_array($current_method,$methods))
		{
			if ($this->CI->session->userdata('current_user'))
			{
				$this->CI->session->set_flashdata('error',"You already logged in");
				redirect(urlto($target));
			}
		}
	}
}