<?php 

class Myview {

	public $theme;

	public function __construct()
	{

	}

	// set a theme: a folder inside view/ dir
	public function setTheme($theme)
	{
		$CI =& get_instance();
		$CI->load->library('mydbconfig');

		if (empty($theme)) {
			$this->theme = $CI->mydbconfig->get('default_theme');
		}else {
			$this->theme = $theme;
		}
	}

	// return current theme
	public function theme()
	{
		return $this->theme;
	}

	/*
	function show()
	
	showing complete content of view

	parameters:
	- $content  : the content of the page
	- $data 	: array of data that will be passed into the page
	- $custom 	: array of configuration
		- header: header filename
		- footer: footer filename
	*/
	public function show($content='default',$data=array(),$custom=array("header"=>'header',"footer"=>"footer"))
	{
		$CI =& get_instance();
		$CI->load->library('mydbconfig');

		$data['company_name'] = $CI->mydbconfig->get('site_company');
		$data['site_name'] = $CI->mydbconfig->get('site_name');

		if ($content == "") {
			$content = "default";
		}

		if ($custom["header"] != NULL ) {
			$CI->load->view( $this->theme."/".$custom['header'], $data );
		}

		$CI->load->view( $this->theme."/".$content,$data);
		
		if ($custom['footer'] != NULL) {
			$CI->load->view( $this->theme."/".$custom['footer'], $data );
		}
	}
}