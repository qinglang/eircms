<?php 


class Mydbconfig
{
	public $CI;

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function get($key)
	{
		$result = $this->CI->db->get_where('config',array('name' => $key));
		if ($result->num_rows() > 0)
		{
			return $result->row()->value;
		}
		else {
			return FALSE;
		}
	}
}